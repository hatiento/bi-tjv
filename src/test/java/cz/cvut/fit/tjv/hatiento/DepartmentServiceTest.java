package cz.cvut.fit.tjv.hatiento;

import cz.cvut.fit.tjv.hatiento.dto.DepartmentCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.DepartmentDTO;
import cz.cvut.fit.tjv.hatiento.entity.Department;
import cz.cvut.fit.tjv.hatiento.repository.DepartmentRepository;
import cz.cvut.fit.tjv.hatiento.service.DepartmentService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;
import static org.mockito.ArgumentMatchers.any;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;

    @MockBean
    private DepartmentRepository departmentRepositoryMock;

    @Test
    void create(){
        Department departmentToReturn = new Department("nameTest", 100);

        // Setting id even without setter or constructor
        ReflectionTestUtils.setField(departmentToReturn, "id", 11);

        DepartmentCreateDTO departmentCreateDTO = new DepartmentCreateDTO("nameTest", 100);

        // departmentToReturn should be returned for ANY department passed to save
        BDDMockito.given(departmentRepositoryMock.save(any(Department.class))).willReturn(departmentToReturn);

        DepartmentDTO returnedDepartmentDTO = departmentService.create(departmentCreateDTO);

        DepartmentDTO expectedDepartmentDTO = new DepartmentDTO(11, "nameTest", 100);
        //assertEquals(expectedDepartmentDTO, returnedDepartmentDTO);
        assertEquals(returnedDepartmentDTO.getId(), 11);
        assertEquals(returnedDepartmentDTO.getDep_name(), "nameTest");
        assertEquals(returnedDepartmentDTO.getDep_room_no(), 100);

        // Checking that department passed to .save() had correct attributes
        ArgumentCaptor<Department> argumentCaptor = ArgumentCaptor.forClass(Department.class);
        Mockito.verify(departmentRepositoryMock, Mockito.atLeastOnce()).save(argumentCaptor.capture());
        Department departmentProvidedToSave = argumentCaptor.getValue();
        assertEquals("nameTest", departmentProvidedToSave.getDep_name());
        assertEquals(100, departmentProvidedToSave.getDep_room_no());
    }
}
