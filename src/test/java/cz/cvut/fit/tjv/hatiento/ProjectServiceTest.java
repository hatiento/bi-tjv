package cz.cvut.fit.tjv.hatiento;

import cz.cvut.fit.tjv.hatiento.dto.ProjectCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.ProjectDTO;
import cz.cvut.fit.tjv.hatiento.entity.Department;
import cz.cvut.fit.tjv.hatiento.entity.Employee;
import cz.cvut.fit.tjv.hatiento.entity.Project;
import cz.cvut.fit.tjv.hatiento.repository.ProjectRepository;
import cz.cvut.fit.tjv.hatiento.service.ProjectService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class ProjectServiceTest {
    @Autowired
    private ProjectService projectService;

    @MockBean
    private ProjectRepository projectRepositoryMock;

    @Test
    void create() throws Exception {
        Department department = new Department("testDepartment", 101);
        List<Employee> employees = new ArrayList<Employee>();
        Employee emp1 = new Employee("John", "Smith", 25, "Programmer", department);
        Employee emp2 = new Employee("Jane", "Smith", 31, "Analyst", department);
        Employee emp3 = new Employee("Peter", "Dun", 42, "Tester", department);

        // Setting id even without setter or constructor
        ReflectionTestUtils.setField(emp1, "id", 11);
        ReflectionTestUtils.setField(emp2, "id", 12);
        ReflectionTestUtils.setField(emp3, "id", 13);

        employees.add(emp1);
        employees.add(emp2);
        employees.add(emp3);

        Project projectToReturn = new Project("testProject", "description", "1.1.2020", null, 50000000, employees);

        // Setting id even without setter or constructor
        ReflectionTestUtils.setField(projectToReturn, "id", 11);

        List<Integer> employeesIds = new ArrayList<Integer>();
        employeesIds.add(11);
        employeesIds.add(12);
        employeesIds.add(13);

        ProjectCreateDTO projectCreateDTO = new ProjectCreateDTO(
                "testProject", "description", "1.1.2020", null, 50000000, employeesIds
        );

        // projectToReturn should be returned for ANY project passed to save
        BDDMockito.given(projectRepositoryMock.save(any(Project.class))).willReturn(projectToReturn);

        ProjectDTO returnedProjectDTO = projectService.create(projectCreateDTO);

        // Option with equals() in ProjectDTO
        ProjectDTO expectedProjectDTO = new ProjectDTO(11, "testProject", "description", "1.1.2020", null, 50000000, employeesIds);
        //assertEquals(expectedProjectDTO, returnedProjectDTO);
        assertEquals(returnedProjectDTO.getId(), 11);
        assertEquals(returnedProjectDTO.getProject_name(), "testProject");
        assertEquals(returnedProjectDTO.getDescription(), "description");
        assertEquals(returnedProjectDTO.getStart_date(), "1.1.2020");
        assertNull(returnedProjectDTO.getEnd_date());
        assertEquals(returnedProjectDTO.getPrice(), 50000000);
        assertEquals(returnedProjectDTO.getEmployeeIds(), employeesIds);

        // Checking that project passed to .save() had correct attributes
        ArgumentCaptor<Project> argumentCaptor = ArgumentCaptor.forClass(Project.class);
        Mockito.verify(projectRepositoryMock, Mockito.atLeastOnce()).save(argumentCaptor.capture());
        Project projectProvidedToSave = argumentCaptor.getValue();
        assertEquals("testProject", projectProvidedToSave.getProject_name());
        assertEquals("description", projectProvidedToSave.getDescription());
        assertEquals("1.1.2020", projectProvidedToSave.getStart_date());
        assertNull(projectProvidedToSave.getEnd_date());
        assertEquals(50000000, projectProvidedToSave.getPrice());
        //assertEquals(employees, projectProvidedToSave.getEmployees());
    }
}
