package cz.cvut.fit.tjv.hatiento;

import cz.cvut.fit.tjv.hatiento.dto.EmployeeCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.EmployeeDTO;
import cz.cvut.fit.tjv.hatiento.entity.Department;
import cz.cvut.fit.tjv.hatiento.entity.Employee;
import cz.cvut.fit.tjv.hatiento.repository.EmployeeRepository;
import cz.cvut.fit.tjv.hatiento.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;


@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @MockBean
    private EmployeeRepository employeeRepositoryMock;

    @Test
    void create() throws Exception {

        //Department department = new Department("testDepartment", 101);
        Employee employeeToReturn = new Employee("John", "Smith", 25, "Programmer");

        // Setting id even without setter or constructor
        ReflectionTestUtils.setField(employeeToReturn, "id", 11);
        //ReflectionTestUtils.setField(department, "id", 21);

        EmployeeCreateDTO employeeCreateDTO = new EmployeeCreateDTO( "John", "Smith",25, "Programmer");

        // employeeToReturn should be returned for ANY author passed to save
        BDDMockito.given(employeeRepositoryMock.save(any(Employee.class))).willReturn(employeeToReturn);

        EmployeeDTO returnedEmployeeDTO = employeeService.create(employeeCreateDTO);

        // Option with equals() in EmployeeDTO
        EmployeeDTO expectedEmployeeDTO = new EmployeeDTO(11, "John", "Smith",25, "Programmer");
        //assertEquals(expectedEmployeeDTO, returnedEmployeeDTO);
        assertEquals(returnedEmployeeDTO.getId(), 11);
        assertEquals(returnedEmployeeDTO.getFirstName(), "John");
        assertEquals(returnedEmployeeDTO.getLastName(), "Smith");
        assertEquals(returnedEmployeeDTO.getAge(), 25);
        assertEquals(returnedEmployeeDTO.getRole(), "Programmer");
        //assertEquals(returnedEmployeeDTO.getDepartmentId(), 21);

        // Checking that employee passed to .save() had correct attributes
        ArgumentCaptor<Employee> argumentCaptor = ArgumentCaptor.forClass(Employee.class);
        Mockito.verify(employeeRepositoryMock, Mockito.atLeastOnce()).save(argumentCaptor.capture());
        Employee employeeProvidedToSave = argumentCaptor.getValue();
        assertEquals("John", employeeProvidedToSave.getFirstName());
        assertEquals("Smith", employeeProvidedToSave.getLastName());
        assertEquals(25, employeeProvidedToSave.getAge());
        assertEquals("Programmer", employeeProvidedToSave.getRole());
        //assertEquals(department, employeeProvidedToSave.getDepartment());

    }

}
