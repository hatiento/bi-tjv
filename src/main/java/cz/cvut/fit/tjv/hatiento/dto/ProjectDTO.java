package cz.cvut.fit.tjv.hatiento.dto;

import cz.cvut.fit.tjv.hatiento.entity.Project;

import java.util.List;

public class ProjectDTO {
    private int id;
    private String project_name;
    private String description;
    private String start_date;
    private String end_date;
    private int price;

    private List<Integer> employeeIds;

    public ProjectDTO(){}

    public ProjectDTO(int id, String project_name, String description, String start_date, int price, List<Integer> employeeIds) {
        this.id = id;
        this.project_name = project_name;
        this.description = description;
        this.start_date = start_date;
        this.end_date = null;
        this.price = price;
        this.employeeIds = employeeIds;
    }

    public ProjectDTO(int id, String project_name, String description, String start_date, String end_date, int price, List<Integer> employeeIds) {
        this.id = id;
        this.project_name = project_name;
        this.description = description;
        this.start_date = start_date;
        this.end_date = end_date;
        this.price = price;
        this.employeeIds = employeeIds;
    }

    public int getId() {
        return id;
    }

    public String getProject_name() {
        return project_name;
    }

    public String getDescription() {
        return description;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public int getPrice() {
        return price;
    }

    public List<Integer> getEmployeeIds() {
        return employeeIds;
    }
}
