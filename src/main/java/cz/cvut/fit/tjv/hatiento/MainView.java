package cz.cvut.fit.tjv.hatiento;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import com.vaadin.flow.router.Route;

/**
 * A sample Vaadin view class.
 * <p>
 * To implement a Vaadin view just extend any Vaadin component and
 * use @Route annotation to announce it in a URL as a Spring managed
 * bean.
 * Use the @PWA annotation make the application installable on phones,
 * tablets and some desktop browsers.
 * <p>
 * A new instance of this class is created for every new user and every
 * browser tab/window.
 */
@Route
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
public class MainView extends VerticalLayout {
    public MainView() {
        Response r = new Response();

        HorizontalLayout getBtns = new HorizontalLayout();
        VerticalLayout data = new VerticalLayout();

        Button getProjects = new Button("GET projects", event ->{
            r.getMethod("http://localhost:8080//project/all", data);

            Form.addButtons(data, r, "Project");
        });

        Button getEmployees = new Button("GET employees", event -> {
            r.getMethod("http://localhost:8080//employee/all", data);

            Form.addButtons(data, r, "Employee");
        });

        Button getDepartments = new Button("GET departments", event -> {
            r.getMethod("http://localhost:8080//department/all", data);
        });

        getEmployees.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        getProjects.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        getDepartments.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        getBtns.add(getProjects, getDepartments, getEmployees);
        add(getBtns);
        add(data);
    }
}
