package cz.cvut.fit.tjv.hatiento.dto;

public class EmployeeCreateDTO {
    private String firstName;
    private String lastName;
    private int age;
    private String role;

    private Integer departmentId;

    public EmployeeCreateDTO(){}

    public EmployeeCreateDTO(String firstName, String lastName, int age, String role){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.role = role;
        this.departmentId = null;
    }

    public EmployeeCreateDTO(String firstName, String lastName, int age, String role, Integer departmentId){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.role = role;
        this.departmentId = departmentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getRole() {
        return role;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }
}
