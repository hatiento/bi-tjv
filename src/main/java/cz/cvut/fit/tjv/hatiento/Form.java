package cz.cvut.fit.tjv.hatiento;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

import java.util.*;

public final class Form {
    public static void addButtons(VerticalLayout data, Response r, String type){
        HorizontalLayout buttons = new HorizontalLayout();

        Button Add = new Button("Add " + type, e ->{
            if(type.equals("Project"))
                Form.addProjectForm(data, r);
            else
                Form.addEmployeeForm(data, r);
        });

        Button Edit = new Button("Edit " + type, e ->{
            if(type.equals("Project"))
                Form.editProjectForm(data, r);
            else
                Form.editEmployeeForm(data, r);
        });

        Button Delete = new Button("Delete " + type, e->{
            if(type.equals("Project"))
                Form.deleteProjectForm(data, r);
            else
                Form.deleteEmployeeForm(data, r);
        });

        buttons.add(Add, Edit, Delete);
        data.add(buttons);
    }


    public static void deleteProjectForm(VerticalLayout data, Response r){
        FormLayout projectLayout = new FormLayout();
        TextField projectId = new TextField("Project ID");

        Button confirm = new Button("OK", e->{
            r.deleteMethod("http://localhost:8080//project/" + projectId.getValue());
            r.getMethod("http://localhost:8080//project/all", data);
            addButtons(data, r, "Project");
        });

        projectLayout.add(projectId, confirm);
        data.add(projectLayout);
    }

    public static void deleteEmployeeForm(VerticalLayout data, Response r){
        FormLayout employeeLayout = new FormLayout();
        TextField employeeId = new TextField("Employee ID");

        Button confirm = new Button("OK", e->{
           r.deleteMethod("http://localhost:8080//employee/" + employeeId.getValue());
           r.getMethod("http://localhost:8080//employee/all", data);
           addButtons(data, r, "Employee");
        });

        employeeLayout.add(employeeId, confirm);
        data.add(employeeLayout);
    }

    public static void editEmployeeForm(VerticalLayout data, Response r){
        Map<String, String> params = new HashMap<String, String>();
        FormLayout employeeLayout = new FormLayout();

        TextField employeeId = new TextField("Employee ID");
        TextField firstName = new TextField("First name");
        TextField lastName = new TextField("Last name");
        TextField age = new TextField("Age");
        TextField role = new TextField("Role");
        TextField departmentId = new TextField("Department ID");
        Button confirm = new Button("OK", e -> {

            params.put("firstName", firstName.getValue());
            params.put("lastName", lastName.getValue());
            params.put("age", age.getValue());
            params.put("role", role.getValue());
            params.put("departmentId", departmentId.getValue());

            r.putMethod("http://localhost:8080//employee/" + employeeId.getValue(), params);
            r.getMethod("http://localhost:8080//employee/all", data);
            addButtons(data, r, "Employee");
        });

        employeeLayout.add(employeeId, firstName, lastName, age, role, departmentId, confirm);
        data.add(employeeLayout);
    }


    public static void addEmployeeForm(VerticalLayout data, Response r){
        Map<String, String> params = new HashMap<String, String>();
        FormLayout employeeLayout = new FormLayout();

        TextField firstName = new TextField("First name");
        TextField lastName = new TextField("Last name");
        TextField age = new TextField("Age");
        TextField role = new TextField("Role");
        TextField departmentId = new TextField("Department ID");
        Button confirm = new Button("OK", e -> {

            params.put("firstName", firstName.getValue());
            params.put("lastName", lastName.getValue());
            params.put("age", age.getValue());
            params.put("role", role.getValue());
            params.put("departmentId", departmentId.getValue());

            r.postMethod("http://localhost:8080//employee", params);
            r.getMethod("http://localhost:8080//employee/all", data);
            addButtons(data, r, "Employee");
        });

        employeeLayout.add(firstName, lastName, age, role, departmentId, confirm);
        data.add(employeeLayout);
    }

    public static void editProjectForm(VerticalLayout data, Response r){
        Map<String, String> params = new HashMap<String, String>();
        FormLayout employeeLayout = new FormLayout();

        TextField projectId = new TextField("Project ID");
        TextField projectName = new TextField("Project name");
        TextField description = new TextField("Description");
        DatePicker startDate = new DatePicker();
        DatePicker endDate = new DatePicker();
        startDate.setLabel("Start date");
        endDate.setLabel("End date");
        TextField price = new TextField("Price");
        MultiSelectListBox<Integer> employeeList = new MultiSelectListBox<>();

        ArrayList<Integer> employeeIds = r.getMethod("http://localhost:8080/employee/all");
        employeeList.setItems(employeeIds);

        Button confirm = new Button("OK", e -> {
            params.put("project_name", projectName.getValue());
            params.put("description", description.getValue());
            try{
                params.put("start_date", startDate.getValue().toString());
                params.put("end_date", endDate.getValue().toString());
            }catch (Exception exception){
                System.out.println("Date is null");
            }
            params.put("price", price.getValue());

            Set<Integer> items = new HashSet<>(employeeList.getSelectedItems());
            StringBuilder empIds = new StringBuilder("[");

            int l = items.size();
            int i = 0;
            for(Integer id : items){
                empIds.append(id);
                i++;

                if(i < l){
                    empIds.append(",");
                }
            }

            empIds.append("]");
            System.out.println(empIds.toString());

            params.put("employeeIds", empIds.toString());

            r.putMethod("http://localhost:8080//project/" + projectId.getValue(), params);
            r.getMethod("http://localhost:8080//project/all", data);
            addButtons(data, r, "Project");
        });

        employeeLayout.add(projectId, projectName, description, startDate, endDate, price, employeeList, confirm);
        data.add(employeeLayout);
    }

    public static void addProjectForm(VerticalLayout data, Response r){
        Map<String, String> params = new HashMap<String, String>();
        FormLayout employeeLayout = new FormLayout();

        TextField projectName = new TextField("Project name");
        TextField description = new TextField("Description");
        DatePicker startDate = new DatePicker();
        DatePicker endDate = new DatePicker();
        startDate.setLabel("Start date");
        endDate.setLabel("End date");
        TextField price = new TextField("Price");
        MultiSelectListBox<Integer> employeeList = new MultiSelectListBox<>();

        ArrayList<Integer> employeeIds = r.getMethod("http://localhost:8080/employee/all");
        employeeList.setItems(employeeIds);

        Button confirm = new Button("OK", e -> {
            params.put("project_name", projectName.getValue());
            params.put("description", description.getValue());
            try{
                params.put("start_date", startDate.getValue().toString());
                params.put("end_date", endDate.getValue().toString());
            }catch (Exception exception){
                System.out.println("Date is null");
            }
            params.put("price", price.getValue());

            Set<Integer> items = new HashSet<>(employeeList.getSelectedItems());
            StringBuilder empIds = new StringBuilder("[");

            int l = items.size();
            int i = 0;
            for(Integer id : items){
                empIds.append(id);
                i++;

                if(i < l){
                    empIds.append(",");
                }
            }

            empIds.append("]");
            System.out.println(empIds.toString());

            params.put("employeeIds", empIds.toString());

            r.postMethod("http://localhost:8080//project", params);
            r.getMethod("http://localhost:8080//project/all", data);
            addButtons(data, r, "Project");
        });

        employeeLayout.add(projectName, description, startDate, endDate, price, employeeList, confirm);
        data.add(employeeLayout);
    }
}
