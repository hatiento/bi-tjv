package cz.cvut.fit.tjv.hatiento.service;

import cz.cvut.fit.tjv.hatiento.dto.ProjectCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.ProjectDTO;
import cz.cvut.fit.tjv.hatiento.entity.Employee;
import cz.cvut.fit.tjv.hatiento.entity.Project;
import cz.cvut.fit.tjv.hatiento.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final EmployeeService employeeService;

    @Autowired
    public ProjectService(ProjectRepository projectRepository, EmployeeService employeeService){
        this.projectRepository = projectRepository;
        this.employeeService = employeeService;
    }

    public List<ProjectDTO> findAll() {
        return projectRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public Optional<Project> findById(int id) {
        return projectRepository.findById(id);
    }

    public Optional<ProjectDTO> findByIdAsDTO(int id) {
        return toDTO(findById(id));
    }

    /*public Optional<ProjectDTO> findByName(String project_name) {
        return toDTO(projectRepository.findByProject_name(project_name));
    }*/

    @Transactional
    public void deleteById(int id){
        projectRepository.deleteById(id);
    }

    @Transactional
    public ProjectDTO create(ProjectCreateDTO projectDTO) throws Exception {
        List<Employee> employees = employeeService.findByIds(projectDTO.getEmployeeIds());

        //if (employees.size() != projectDTO.getEmployeeIds().size())
        //    throw new Exception("some employees not found");

        Project project = new Project(
                projectDTO.getProject_name(),
                projectDTO.getDescription(),
                projectDTO.getStart_date(),
                projectDTO.getEnd_date(),
                projectDTO.getPrice(),
                employees
        );

        return toDTO(projectRepository.save(project));
    }

    @Transactional
    public ProjectDTO update(int id, ProjectCreateDTO projectDTO) throws Exception {
        Optional<Project> optionalProject = findById(id);

        if (optionalProject.isEmpty())
            throw new Exception("no such project");

        Project project = optionalProject.get();
        project.setProject_name(projectDTO.getProject_name());
        project.setDescription(projectDTO.getDescription());
        project.setStart_date(projectDTO.getStart_date());
        project.setEnd_date(projectDTO.getEnd_date());
        project.setPrice(projectDTO.getPrice());

        List<Employee> employees = employeeService.findByIds(projectDTO.getEmployeeIds());
        if (employees.size() != projectDTO.getEmployeeIds().size())
            throw new Exception("some authors not found");
        project.setEmployees(employees);


        return toDTO(project);
    }

    private ProjectDTO toDTO(Project project) {
        return new ProjectDTO(
                project.getId(),
                project.getProject_name(),
                project.getDescription(),
                project.getStart_date(),
                project.getEnd_date(),
                project.getPrice(),
                project.getEmployees().stream().map(Employee::getId).collect(Collectors.toList())
        );
    }

    private Optional<ProjectDTO> toDTO(Optional<Project> project) {
        if (project.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(project.get()));
    }
}

