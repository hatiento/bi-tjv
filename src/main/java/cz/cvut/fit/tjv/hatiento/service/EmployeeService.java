package cz.cvut.fit.tjv.hatiento.service;

import cz.cvut.fit.tjv.hatiento.dto.EmployeeCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.EmployeeDTO;
import cz.cvut.fit.tjv.hatiento.entity.Department;
import cz.cvut.fit.tjv.hatiento.entity.Employee;
import cz.cvut.fit.tjv.hatiento.repository.DepartmentRepository;
import cz.cvut.fit.tjv.hatiento.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, DepartmentRepository departmentRepository){
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
    }

    public List<EmployeeDTO> findAll(){
        return employeeRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<Employee> findByIds(List<Integer> ids){
        return employeeRepository.findAllById(ids);
    }

    public Optional<Employee> findById(int id){
        return employeeRepository.findById(id);
    }

    public Optional<EmployeeDTO> findByIdAsDTO(int id){
        return toDTO(findById(id));
    }

    public Optional<Department> findDepById(int id){ return departmentRepository.findById(id); }

    @Transactional
    public void deleteById(int id){
        employeeRepository.deleteById(id);
    }

    @Transactional
    public EmployeeDTO create(EmployeeCreateDTO employeeDTO) throws Exception {

        Department department = employeeDTO.getDepartmentId() == null ? null :
                findDepById(employeeDTO.getDepartmentId()).orElseThrow(() -> new Exception("no such department"));

        Employee employee = new Employee(
                employeeDTO.getFirstName(),
                employeeDTO.getLastName(),
                employeeDTO.getAge(),
                employeeDTO.getRole(),
                department
        );

        return toDTO(employeeRepository.save(employee));
    }

    @Transactional
    public EmployeeDTO update(int id, EmployeeCreateDTO employeeCreateDTO) throws Exception {
        Optional<Employee> optionalEmployee = findById(id);
        if (optionalEmployee.isEmpty())
            throw new Exception("no such Employee");

        Employee employee = optionalEmployee.get();
        employee.setFirstName(employeeCreateDTO.getFirstName());
        employee.setLastName(employeeCreateDTO.getLastName());
        employee.setRole(employeeCreateDTO.getRole());
        employee.setAge(employeeCreateDTO.getAge());

        employee.setDepartment(employeeCreateDTO.getDepartmentId() == null ?
                null :
                findDepById(employeeCreateDTO.getDepartmentId()).orElseThrow(() -> new Exception(
                        "no such department")));

        return toDTO(employee);
    }

    private EmployeeDTO toDTO(Employee employee) {
        return new EmployeeDTO(
                employee.getId(),
                employee.getFirstName(),
                employee.getLastName(),
                employee.getAge(),
                employee.getRole(),
                employee.getDepartment() != null ? employee.getDepartment().getId() : null
                );
    }

    private Optional<EmployeeDTO> toDTO(Optional<Employee> employee) {
        if (employee.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(employee.get()));
    }
}

