package cz.cvut.fit.tjv.hatiento.controller;

import cz.cvut.fit.tjv.hatiento.dto.ProjectCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.ProjectDTO;
import cz.cvut.fit.tjv.hatiento.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/project/all")
    List<ProjectDTO> all(){
        return projectService.findAll();
    }

    @GetMapping("/project/{id}")
    ProjectDTO byId(@PathVariable int id){
        return projectService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /*@GetMapping(name = "/project", params = {"project_name"})
    ProjectDTO byName(@RequestParam String project_name) {
        return projectService.findByName(project_name).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }*/

    @PostMapping("/project")
    ProjectDTO save(@RequestBody ProjectCreateDTO project) throws Exception {
        return projectService.create(project);
    }

    @PutMapping("/project/{id}")
    ProjectDTO save(@PathVariable int id, @RequestBody ProjectCreateDTO book) {
        try{
            return projectService.update(id, book);
        }catch (Exception e){
            System.out.println("Project not found");
            return null;
        }
    }

    @DeleteMapping("/project/{id}")
    public void deleteById(@PathVariable int id) {
        try{
            projectService.deleteById(id);
        }catch(Exception e){
            System.out.println("Project not found");
        }
    }

}
