package cz.cvut.fit.tjv.hatiento;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import cz.cvut.fit.tjv.hatiento.dto.DepartmentDTO;
import cz.cvut.fit.tjv.hatiento.dto.EmployeeDTO;
import cz.cvut.fit.tjv.hatiento.dto.ProjectDTO;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Response{
    public void DepartmentGrid(VerticalLayout data, String responseBody){
        List<DepartmentDTO> departmentList = new ArrayList<>();
        JSONArray jsonarray = new JSONArray(responseBody);

        for(int i = 0; i < jsonarray.length(); i++){
            JSONObject obj = jsonarray.getJSONObject(i);
            System.out.println(obj);
            departmentList.add(new DepartmentDTO(
                    (Integer) obj.get("id"),
                    obj.getString("dep_name"),
                    (Integer) obj.get("dep_room_no")
            ));

            Grid<DepartmentDTO> grid = new Grid<>(DepartmentDTO.class);
            grid.setItems(departmentList);
            grid.setColumns("id", "dep_name", "dep_room_no");
            data.removeAll();
            data.add(grid);
        }
    }

    public void EmployeeGrid(VerticalLayout data, String responseBody){
        List<EmployeeDTO> employeeList = new ArrayList<>();

        JSONArray jsonarray = new JSONArray(responseBody);
        for(int i = 0; i < jsonarray.length(); i++){
            JSONObject obj = jsonarray.getJSONObject(i);
            employeeList.add(new EmployeeDTO(
                    (Integer) obj.get("id"),
                    obj.getString("firstName"),
                    obj.getString("lastName"),
                    (Integer)obj.get("age"),
                    obj.getString("role"),
                    (Integer)obj.get("departmentId")
                    ));
        }

        Grid<EmployeeDTO> grid = new Grid<>(EmployeeDTO.class);
        grid.setItems(employeeList);
        grid.setColumns("id", "firstName", "lastName", "age", "role", "departmentId");
        data.removeAll();
        data.add(grid);
    }

    public void ProjectGrid(VerticalLayout data, String responseBody){
        List<ProjectDTO> projectList = new ArrayList<>();

        JSONArray jsonarray = new JSONArray(responseBody);
        for(int i = 0; i < jsonarray.length(); i++){
            JSONObject obj = jsonarray.getJSONObject(i);

            ArrayList<Integer> employeeIds = new ArrayList<>();

            String employees = obj.get("employeeIds").toString();
            employees = employees.substring(1, employees.length() - 1);

            employees = employees.replaceAll("[^-?0-9]+", " ");
            String[] spl = employees.split(" ");

            for(String id : spl){
                employeeIds.add(Integer.parseInt(id));
            }

            projectList.add(new ProjectDTO(
                    (Integer)obj.get("id"),
                    obj.getString("project_name"),
                    obj.getString("description"),
                    obj.get("start_date").toString(),
                    obj.get("end_date").toString(),
                    (Integer)obj.get("price"),
                    employeeIds
            ));
        }

        Grid<ProjectDTO> grid = new Grid<>(ProjectDTO.class);
        grid.setItems(projectList);
        grid.setColumns("id", "project_name", "description", "start_date", "end_date", "price", "employeeIds");
        data.removeAll();
        data.add(grid);
    }

    public void getMethod(String url, VerticalLayout data){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpresponse = httpClient.execute(httpGet);
            String responseBody = EntityUtils.toString(httpresponse.getEntity(), StandardCharsets.UTF_8);

            if(url.contains("employee"))
                EmployeeGrid(data, responseBody);

            else if(url.contains("project"))
                ProjectGrid(data, responseBody);

            else
                DepartmentGrid(data, responseBody);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Integer> parse(String responseBody){
        ArrayList<Integer> ids = new ArrayList<>();
        String json = "{\"employees\":" + responseBody + "}";

        JSONObject obj = new JSONObject(json);
        JSONArray jsonArray = (JSONArray) obj.get("employees");
        for(int i = 0; i < jsonArray.length(); i++){
            ids.add(Integer.parseInt(jsonArray.getJSONObject(i).get("id").toString()));
        }

        return ids;
    }

    public ArrayList<Integer> getMethod(String url){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse httpresponse = httpClient.execute(httpGet);
            String responseBody = EntityUtils.toString(httpresponse.getEntity());

            //if(httpresponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
            return parse(responseBody);
            //}

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void deleteMethod(String url){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpDelete httpDelete = new HttpDelete(url);

        try{
            httpClient.execute(httpDelete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String parseParams(Map<String, String> params){
        String jsonBody = "{";

        int length = params.size();
        int i = 0;

        for(Map.Entry<String, String> entry : params.entrySet()){
            if(entry.getKey().equals("employeeIds"))
                jsonBody = jsonBody + "\"" + entry.getKey() + "\":" + entry.getValue();

            else
                jsonBody = jsonBody + "\"" + entry.getKey() + "\":" + "\"" + entry.getValue() + "\"";

            i++;

            if(i < length)
                jsonBody += ",";
        }

        return jsonBody + "}";
    }

    public void putMethod(String url, Map<String, String> params){
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(url);
        String jsonBody = parseParams(params);

        try {
            StringEntity entity = new StringEntity(jsonBody);
            httpPut.setEntity(entity);
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = httpClient.execute(httpPut);
            System.out.println("Response: " + response);
            httpClient.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public void postMethod(String url, Map<String, String> params) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        String jsonBody = parseParams(params);
        System.out.println(jsonBody);

        try {
            StringEntity entity = new StringEntity(jsonBody);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = httpClient.execute(httpPost);
            System.out.println("Response: " + response);
            httpClient.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
