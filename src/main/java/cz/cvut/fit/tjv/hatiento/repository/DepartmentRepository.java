package cz.cvut.fit.tjv.hatiento.repository;

import cz.cvut.fit.tjv.hatiento.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer>{
}
