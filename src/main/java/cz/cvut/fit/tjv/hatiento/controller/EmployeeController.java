package cz.cvut.fit.tjv.hatiento.controller;

import cz.cvut.fit.tjv.hatiento.dto.EmployeeCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.EmployeeDTO;
import cz.cvut.fit.tjv.hatiento.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService){
        this.employeeService = employeeService;
    }

    @GetMapping("/employee/all")
    List<EmployeeDTO> all(){
        return employeeService.findAll();
    }

    @GetMapping("/employee/{id}")
    EmployeeDTO byId(@PathVariable int id) {
        return employeeService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/employee")
    EmployeeDTO save(@RequestBody EmployeeCreateDTO employee) throws Exception {
        return employeeService.create(employee);
    }

    @PutMapping("/employee/{id}")
    EmployeeDTO save(@PathVariable int id, @RequestBody EmployeeCreateDTO employee) {
        try {
            return employeeService.update(id, employee);
        }catch(Exception e){
            System.out.println("Employee not found.");
            return null;
        }

    }

    @DeleteMapping("/employee/{id}")
    public void deleteById(@PathVariable int id) {
        try{
            employeeService.deleteById(id);
        }catch (Exception e){
            System.out.println("Employee not found.");
        }
    }
}
