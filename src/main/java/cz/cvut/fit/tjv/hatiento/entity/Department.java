package cz.cvut.fit.tjv.hatiento.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity // JPA Entity
public class Department {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String dep_name;

    @NotNull
    private int dep_room_no;

    public Department() {
    }

    public Department(String dep_name, int dep_room_no){
        this.dep_name = dep_name;
        this.dep_room_no = dep_room_no;
    }

    public int getId(){
        return id;
    }

    public String getDep_name(){
        return dep_name;
    }

    public void setDep_name(String dep_name){
        this.dep_name = dep_name;
    }

    public int getDep_room_no(){
        return dep_room_no;
    }

    public void setDep_room_no(int dep_room_no){
        this.dep_room_no = dep_room_no;
    }
}
