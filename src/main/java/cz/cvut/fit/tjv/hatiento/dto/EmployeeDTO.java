package cz.cvut.fit.tjv.hatiento.dto;

import cz.cvut.fit.tjv.hatiento.entity.Employee;

public class EmployeeDTO {
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private String role;

    private Integer departmentId;

    public EmployeeDTO(){}

    public EmployeeDTO(int id, String firstName, String lastName, int age, String role){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.role = role;
        this.departmentId = null;
    }

    public EmployeeDTO(int id, String firstName, String lastName, int age, String role, Integer departmentId){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.role = role;
        this.departmentId = departmentId;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getRole() {
        return role;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }
}
