package cz.cvut.fit.tjv.hatiento.controller;

import cz.cvut.fit.tjv.hatiento.dto.DepartmentCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.DepartmentDTO;
import cz.cvut.fit.tjv.hatiento.service.DepartmentService;
import org.hibernate.boot.spi.InFlightMetadataCollector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class DepartmentController {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService){
        this.departmentService = departmentService;
    }

    @GetMapping("/department/all")
    List<DepartmentDTO> all(){
        return departmentService.findAll();
    }

    @GetMapping("/department/{id}")
    DepartmentDTO byId(@PathVariable int id) {
        return departmentService.findByIdAsDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/department")
    DepartmentDTO save(@RequestBody DepartmentCreateDTO department) {
        return departmentService.create(department);
    }

    @PutMapping("/department/{id}")
    DepartmentDTO save(@PathVariable int id, @RequestBody DepartmentCreateDTO department) {
        try{
            return departmentService.update(id, department);
        }catch (Exception e){
            System.out.println("Department not found");
            return null;
        }
    }

    @DeleteMapping("/department/{id}")
    public void deleteById(@PathVariable int id) {
        try{
            departmentService.deleteById(id);
        }catch (Exception e){
            System.out.println("Department not found.");
        }
    }

}
