package cz.cvut.fit.tjv.hatiento.repository;

import cz.cvut.fit.tjv.hatiento.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}
