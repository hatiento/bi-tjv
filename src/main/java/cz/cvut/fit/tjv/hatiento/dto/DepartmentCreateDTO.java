package cz.cvut.fit.tjv.hatiento.dto;

public class DepartmentCreateDTO {
    private String dep_name;
    private int dep_room_no;

    public  DepartmentCreateDTO(){}

    public DepartmentCreateDTO(String dep_name, int dep_room_no) {
        this.dep_name = dep_name;
        this.dep_room_no = dep_room_no;
    }

    public String getDep_name() {
        return dep_name;
    }

    public int getDep_room_no() {
        return dep_room_no;
    }
}
