package cz.cvut.fit.tjv.hatiento.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity // JPA Entity
public class Employee {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private int age;

    @NotNull
    private String role;

    @ManyToOne
    private Department department;

    public Employee(){
    }

    public Employee(String firstName, String lastName, int age, String role){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.role = role;
    }

    public Employee(String firstName, String lastName, int age, String role, Department department){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.role = role;
        this.department = department;
    }

    public int getId(){
        return id;
    }

    public String getFirstName(){
        return firstName;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age){
        this.age = age;
    }

    public String getRole(){
        return role;
    }

    public void setRole(String role){
        this.role = role;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}