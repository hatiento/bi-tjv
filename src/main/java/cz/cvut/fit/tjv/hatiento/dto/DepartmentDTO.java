package cz.cvut.fit.tjv.hatiento.dto;

public class DepartmentDTO {
    private int id;
    private String dep_name;
    private int dep_room_no;

    public DepartmentDTO(){}

    public DepartmentDTO(int id, String dep_name, int dep_room_no) {
        this.id = id;
        this.dep_name = dep_name;
        this.dep_room_no = dep_room_no;
    }

    public int getId() {
        return id;
    }

    public String getDep_name() {
        return dep_name;
    }

    public int getDep_room_no() {
        return dep_room_no;
    }
}
