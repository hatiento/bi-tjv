package cz.cvut.fit.tjv.hatiento.repository;

import cz.cvut.fit.tjv.hatiento.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
    //Optional<Project> findByProject_name(String project_name);
}
