package cz.cvut.fit.tjv.hatiento.entity;

import com.sun.istack.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.List;

// AddEmployee, removeEmployee?

@Entity // JPA Entity
public class Project {
    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private String project_name;

    @NotNull
    private String description;

    @DateTimeFormat
    @NotNull
    private String start_date;

    @DateTimeFormat
    private String end_date;

    @NotNull
    private int price;

    @ManyToMany
    @JoinTable(name = "project_employee",
               joinColumns = @JoinColumn(name = "project_id"),
               inverseJoinColumns = @JoinColumn(name = "employee_id")
    )
    private List<Employee> employees;

    public Project(){
    }

    public Project(String project_name, String description, String start_date, int price, List<Employee> employees){
        this.project_name = project_name;
        this.description = description;
        this.start_date = start_date;
        this.price = price;
        this.employees = employees;
    }

    public Project(String project_name, String description, String start_date, String end_date, int price, List<Employee> employees){
        this.project_name = project_name;
        this.description = description;
        this.start_date = start_date;
        this.end_date = end_date;
        this.price = price;
        this.employees = employees;
    }

    public int getId(){
        return id;
    }

    public String getProject_name(){
        return project_name;
    }

    public void setProject_name(String project_name){
        this.project_name = project_name;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getStart_date(){
        return start_date;
    }

    public void setStart_date(String start_date){
        this.start_date = start_date;
    }

    public String getEnd_date(){
        return end_date;
    }

    public void setEnd_date(String end_date){
        this.end_date = end_date;
    }

    public int getPrice(){
        return price;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public List<Employee> getEmployees(){
        return employees;
    }

    public void setEmployees(List<Employee> employees){
        this.employees = employees;
    }
}
