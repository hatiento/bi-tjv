package cz.cvut.fit.tjv.hatiento.service;

import cz.cvut.fit.tjv.hatiento.dto.DepartmentCreateDTO;
import cz.cvut.fit.tjv.hatiento.dto.DepartmentDTO;
import cz.cvut.fit.tjv.hatiento.entity.Department;
import cz.cvut.fit.tjv.hatiento.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository){
        this.departmentRepository = departmentRepository;
    }

    public List<DepartmentDTO> findAll(){
        return departmentRepository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
    }

    public List<Department> findByIds(List<Integer> ids) {
        return departmentRepository.findAllById(ids);
    }

    public Optional<Department> findById(int id) {
        return departmentRepository.findById(id);
    }

    public Optional<DepartmentDTO> findByIdAsDTO(int id) {
        return toDTO(findById(id));
    }

    public DepartmentDTO create(DepartmentCreateDTO departmentDTO) {
        return toDTO(departmentRepository.save(
                new Department(
                        departmentDTO.getDep_name(),
                        departmentDTO.getDep_room_no()
                )
        ));
    }

    @Transactional
    public void deleteById(int id){
        departmentRepository.deleteById(id);
    }

    @Transactional
    public DepartmentDTO update(int id, DepartmentCreateDTO departmentDTO) throws Exception{
        Optional<Department> optionalDepartment = departmentRepository.findById(id);

        if(optionalDepartment.isEmpty())
            throw new Exception(("No such Department."));

        Department department = optionalDepartment.get();
        department.setDep_name(departmentDTO.getDep_name());
        department.setDep_room_no(departmentDTO.getDep_room_no());
        return toDTO(department);
    }

    private DepartmentDTO toDTO(Department department){
        return new DepartmentDTO(department.getId(), department.getDep_name(), department.getDep_room_no());
    }

    private Optional<DepartmentDTO> toDTO(Optional<Department> department){
        if(department.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(department.get()));
    }

}
